Categories:System
License:WTFPL
Web Site:https://github.com/dotWee/MicroPinner/blob/HEAD/README.md
Source Code:https://github.com/dotWee/MicroPinner
Issue Tracker:https://github.com/dotWee/MicroPinner/issues

Auto Name:MicroPinner
Summary:Test and customize the statusbar
Description:
Lightweight dialog-only application, which lets you pin text to your
statusbar. You can also customize the pins priority and visibility
(Android 5.+ only).

Might send crash reports.
.

Repo Type:git
Repo:https://github.com/dotWee/MicroPinner

Build:v1.5.1,16
    commit=release-v1.5.1
    subdir=app
    gradle=yes

Build:v1.5.2,17
    commit=release-v1.5.2
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:v1.5.2
Current Version Code:17

